/*
 *   FUNCTIONS
 */

// equalHeight
function equalHeight(el) {
    var maxHeight = 0;

    $(el).each(function(index, el) {
        var realHeight = $(el).outerHeight();

        if (realHeight > maxHeight) {
            maxHeight = realHeight;
        }
        $(el).height(maxHeight);
    });
}
// END:equalHeight

// toggleClass
function toggleClassElement(element) {
    $(element).toggleClass('active');
}
// END:toggleClass

/*
 *   END:FUNCTIONS
 */

/*
 *   DOMready
 */
$(document).ready(function() {
    var registerUserType = $('.register__user-type');
    var headerChoseList = $('.header-chose__submnenu');
    var headerChoseMain = $('.header-chose__submnenu--main');
    var headerChoseSubmenuWrapper = $('#headerChoseSubmenuWrapper');
    var headerChoseDynamic = $('.header-chose__dynamic');
    var homeAddProductsBtn = $('#homeAddProductsBtn');

    // homeAddProductsBtn
    homeAddProductsBtn.on('click', function(event) {
        event.preventDefault();
        toggleClassElement(headerChoseSubmenuWrapper);
        $('html, body').animate({
            scrollTop: headerChoseSubmenuWrapper.offset().top
        },500);
    });

    $('body').mouseup(function(event) {
        if (!homeAddProductsBtn.is(event.target) && headerChoseSubmenuWrapper.has(event.target).length === 0) {
            headerChoseSubmenuWrapper.removeClass('active');
        }
    });
    // END:homeAddProductsBtn

    // register__user-type
    registerUserType.find('input:checked').parents('label').addClass('active');
    // END:register__user-type

    // bootstrap-select
    $('select').selectpicker({
        style: 'btn-default',
    });
    // END:bootstrap-select

    // input-counter
    $('.input-counter__field').styler({
        
    });
    // END:input-counter
});
/*
 *   END:DOMready
 */

/*
 *   LOAD
 */
$(window).on('load', function(event) {
    var thumbnail = $('.thumbnail');
    var headerChoseDynamic = $('.header-chose__dynamic');
    var headerChoseBtn = $('.header-chose__btn');

    // thumbnails equal height
    equalHeight(thumbnail);
    // END:thumbnails equal height
});
/*
 *   END:LOAD
 */

/*
 *   RESIZE
 */
$(window).on('resize', function(event) {
    // thumbnails equal height
    equalHeight(thumbnail);
    // END:thumbnails equal height

    // headerChoseDynamic equal height
    equalHeight(headerChoseDynamic);
    // END:headerChoseDynamic equal height
});
/*
 *   END:RESIZE
 */
