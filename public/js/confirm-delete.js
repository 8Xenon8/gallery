$(function(){
	$('#confirm-delete').on('click', '.btn-ok', function(e) {
		var $modalDiv = $(e.delegateTarget);
		var href = $(this).data('href');
		var redirect = $(this).data('redirect');
		if(href){
			$.post($(this).data('href')).done(function(){
				window.location = (redirect) ? redirect : '';
			});
		}
		$modalDiv.addClass('loading');
		setTimeout(function() {
			$modalDiv.modal('hide').removeClass('loading');
		}, 1000)
	});
	$('#confirm-delete').on('show.bs.modal', function(e) {
		var data = $(e.relatedTarget).data();
		$('.title', this).text(data.recordTitle);
		$('.btn-ok', this).data('href', data.href);
		console.log(data.redirect);
		if(data.redirect)
			$('.btn-ok', this).data('redirect', data.redirect);
	});
});