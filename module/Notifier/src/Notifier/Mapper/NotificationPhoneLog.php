<?php
namespace Notifier\Mapper;

use Admin\Mapper\AbstractDbMapperWithEntity;
use Notifier\Service\SmsRuErrorListTrait;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Debug\Debug;
class NotificationPhoneLog extends AbstractDbMapperWithEntity
{
    use SmsRuErrorListTrait;

    protected $tableName = 'notification_phone_log';

    public function insert($entity, $tableName = NULL, HydratorInterface $hydrator = NULL)
    {
        if (is_array($entity) && empty($entity['message'])) {
            $entity['message'] = $this->sms_error_list[$entity['status']];
        }

        return parent::insert($entity, $tableName, $hydrator);
    }
}

