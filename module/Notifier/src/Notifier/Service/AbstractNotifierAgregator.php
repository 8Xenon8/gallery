<?php
namespace Notifier\Service;

use Notifier\Service\NotifierItemInterface;

abstract class AbstractNotifierAgregator
{
    protected $errors    = [];
    protected $notifiers = [];
    
    
    public function addNotifier($name, NotifierItemInterface $notifier)
    {
        $this->notifiers[$name] = $notifier; 
    }
    
    public function getNotifier($notifier = null)
    {
        if($notifier && is_scalar($notifier)){
            if(isset($this->notifiers[$notifier])){
                return $this->notifiers[$notifier];
            }
        }
        
        return null;
    }
    public function notify()
    {
        $this->errors = [];
        
        foreach($this->notifiers as $notify){
            $notify->notify();
            if($notify->getErrors())
                $this->errors[get_class($notify)] = $notify->getErrors();
        }
        
        return $this->errors;
    }
    
    public function getErrors(){
        return $this->errors;
    }
}

