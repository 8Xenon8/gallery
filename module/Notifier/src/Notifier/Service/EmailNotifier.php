<?php
namespace Notifier\Service;

use Zend\Mail\Message;
use MtMail\Service\Mail;
use Zend\Mail\Headers;

class EmailNotifier extends AbstractNotifier
{
    
    protected $email_data = [
        'Subject'     => null,
        'To'          => null,
        'From'        => null,
        'Body'        => null,
        'Encoding'    => 'UTF-8'
    ];
        
    protected $mailService = null;
    
    protected $headers     = [
        'Mime-Version' => '1.0',
        'Content-type' => 'text/html; charset="UTF-8"',
    ];
        
    public static $ERR_EMPTY_SUBJECT     = 'Вы не указали тему!';
    public static $ERR_EMPTY_TO          = 'Вы не указали адресата!';
    public static $ERR_EMPTY_FROM        = 'Вы не указали отправителя!';
    public static $ERR_EMPTY_MESSAGE     = 'Вы не ввели сообщение!';
    public static $ERR_EMPTY_MAILSERVICE = 'Вы не указали сервис отправки почты!';
    
    public function notify()
    {
        $this->verifyMailData();
        
        if(!$this->errors)
            $this->sendNotification();
        
        return $this->errors;
    }
    
    /**
     * @return the $subject
     */
    public function getSubject()
    {
        return $this->email_data['Subject'];
    }
    
    /**
     * @return the $to
     */
    public function getTo()
    {
        return $this->email_data['To'];
    }
    
    /**
     * @return the $message
     */
    
    public function getMessage(){
        return $this->email_data['Body'];
    }
    
    /**
     * @return the $from
     */
    public function getFrom()
    {
        return $this->email_data['From'];
    }
    
    /**
     * @return the $mailService
     */
    public function getMailService()
    {
        return $this->mailService;
    }
    
    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->email_data['Subject'] = $subject;
    }
    
    /**
     * @param field_type $to
     */
    public function setTo($to)
    {
        $this->email_data['To'] = $to;
    }
    
    /**
     * @param fiels_type $message
     */
    
    public function setMessage($message){
        $this->email_data['Body'] = $message;
    }
    
    /**
     * @param field_type $from
     */
    public function setFrom($from)
    {
        $this->email_data['From'] = $from;
    }
    
    /**
     * @param MtMail\Service\Mail $mailService
     */
    public function setMailService(Mail $mailService)
    {
        $this->mailService = $mailService;
    }
     
    protected function verifyMailData(){
        if(is_null($this->email_data['Subject'])){
            $this->setError(EmailNotifier::$ERR_EMPTY_SUBJECT);
        }
        if(is_null($this->email_data['To'])){
            $this->setError(EmailNotifier::$ERR_EMPTY_TO);
        }
        if(is_null($this->email_data['Body'])){
            $this->setError(EmailNotifier::$ERR_EMPTY_MESSAGE);
        }
        if(is_null($this->email_data['From'])){
            $this->setError(EmailNotifier::$ERR_EMPTY_FROM);
        }
        if(is_null($this->mailService)){
            $this->setError(EmailNotifier::$ERR_EMPTY_MAILSERVICE);
        }
        
        return $this->errors;
    }
    
    protected function sendNotification(){
        $message = new Message();
        $headers = new Headers();
        $headers->addHeaders($this->headers);
    
        $message->setHeaders($headers);
        foreach($this->email_data as $key=>$value)
            $message->{'set'.$key}($value);
    
            $this->mailService->send($message);
    }
    
  
}

