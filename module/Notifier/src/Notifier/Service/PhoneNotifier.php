<?php
namespace Notifier\Service;

use Notifier\Service\SmsRuErrorListTrait;
use GuzzleHttp;
use Zend\Debug\Debug;
use Notifier\Mapper\NotificationPhoneLog;

/*
 * Класс для работы с api сайта sms.ru
 */
class PhoneNotifier extends AbstractNotifier
{
    use SmsRuErrorListTrait;
       
    protected $success_response = '100';
    
    protected $sms_data = [
        'api_id'  => null,
        'text'    => null,
        //'from'    => null,
        //'test'    => 1,   //Для тестов! Смс не придет на телефон, но ответ от сервера получим, буд-то реально отправляем смс
     ];   
    
    protected $to = null;
    
    protected $api_url = 'http://sms.ru/sms/send?';
        
    protected $log = [
        'id'          => null,
        'status'      => null,
        'message'     => null,
        'description' => null,
    ];
    
    public static $ERR_EMPTY_TO      = 'Вы не ввели адресата!';
    public static $ERR_EMPTY_API_ID  = 'Вы не ввели api_id!';
    public static $ERR_EMPTY_MESSAGE = 'Вы не ввели сообщение!';
    public static $ERR_EMPTY_FROM    = 'Вы не ввели отправителя!';
    
    public function notify()
    {
        $this->sendMessage();
        return $this->getErrors();
    }
     
    public function generateSendData()
    {
        $data = $this->sms_data;
        $data = \Zend\Stdlib\ArrayUtils::merge($data, $this->getPhonesToMultipleUrl());
        
        return $data;
    }
    
    public function setError($error_code, $error = null)
    {
        $this->errors[$error_code] = $error;
    }
    
    public function getPhonesToMultipleUrl()
    {

        $multiple_to = [];
        
        foreach($this->to as $phone){
            $multiple_to['multi[' . $phone . ']='] = $this->getMessage();
        }
        
        return $multiple_to;
    }
    
    
    /**
     * 
     * @return the $log
     */
    public function getLog(){
        return $this->log;
    }
    /**
     *
     * @return the $to
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     *
     * @return the $api_id
     */
    public function getApiId()
    {
        return $this->sms_data['api_id'];
    }

    /**
     *
     * @return the $message
     */
    public function getMessage()
    {
        return $this->sms_data['text'];
    }

    /**
     *
     * @return the $from
     */
    public function getFrom()
    {
        return $this->sms_data['from'];
    }

    /**
     *
     * @param field_type $to            
     */
    public function setTo($to)
    {           
        $this->to = (array) $to;
    }

    /**
     *
     * @param field_type $api_id            
     */
    public function setApiId($api_id)
    {
        $this->sms_data['api_id'] = $api_id;
    }

    /**
     *
     * @param field_type $message            
     */
    public function setMessage($message)
    {
        $this->sms_data['text'] = $message;
    }

    /**
     *
     * @param field_type $from            
     */
    public function setFrom($from)
    {
        $this->sms_data['from'] = $from;
    }
   
    protected function log($id, $status, $message, $description = null)
    {
        $this->log = [  
                'id'          => $id,
                'status'      => $status,
                'message'     => $message,
                'description' => $description,            
        ];
    }
    
    protected function sendMessage()
    {    
        $guzzle   = new GuzzleHttp\Client(['base_uri' => 'http://sms.ru']);
        $response = $guzzle->post('/sms/send', ['form_params' => $this->generateSendData()]);
    
        $sended_response = preg_split("#\s+|(?:\r\n)+#", $response->getBody());
        echo $response->getBody();
    
        if($sended_response[0] != $this->success_response){
            $this->setError($sended_response[0], $this->sms_error_list[$sended_response[0]]);
        }
        if(!isset($sended_response[1]))
            $sended_response[1] = 'NOT_SENDED';
        
        $this->log($sended_response[1], $sended_response[0], null, $this->sms_data['text']);
    }
    
    
}

