<?php
namespace Notifier\Service;

interface NotifierItemInterface
{
    public function notify();
    public function getErrors();
}

