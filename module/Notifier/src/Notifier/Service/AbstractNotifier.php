<?php
namespace Notifier\Service;

use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractNotifier implements NotifierItemInterface
{
    use ServiceLocatorAwareTrait;
    
    protected $message = null;
    
    protected $errors = [];
    
    /**
     * @return the $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param field_type $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * {@inheritDoc}
     * @see \Notifier\Service\NotifierItemInterface::getErrors()
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    public function setError($error)
    {
        return $this->errors[] = $error;
    }

    /**
     * {@inheritDoc}
     * @see \Notifier\Service\NotifierItemInterface::notify()
     * Метод оповещения (вызываеться, когда нужно будет оповестить пользователя)
     */
    abstract function notify();   
}

