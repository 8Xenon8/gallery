-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 07, 2016 at 04:36 PM
-- Server version: 5.6.26-log
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user_easy_buy`
--

-- --------------------------------------------------------

--
-- Table structure for table `notification_phone_log`
--

CREATE TABLE `notification_phone_log` (
  `id` varchar(20) NOT NULL,
  `status` smallint(6) NOT NULL,
  `message` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `shop_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification_phone_log`
--

INSERT INTO `notification_phone_log` (`id`, `status`, `message`, `description`, `shop_id`, `date`) VALUES
('NOT SENDED', 201, 'Не хватает средств на лицевом счету', 'Какое-то описание', 12, '2016-09-07 13:36:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notification_phone_log`
--
ALTER TABLE `notification_phone_log`
  ADD UNIQUE KEY `status` (`status`),
  ADD KEY `id` (`id`),
  ADD KEY `shop_id` (`shop_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
