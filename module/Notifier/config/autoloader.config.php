<?php 
    namespace Notifier;

    return [
        'Zend\Loader\StandardAutoloader' => array(
            'namespaces' => array(
                __NAMESPACE__ => __DIR__ . '/../src/' . __NAMESPACE__,
            )
        )
    ];
?>