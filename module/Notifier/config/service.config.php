<?php 
    namespace Notifier;
    
    use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
                                
    return [
        'invokables' => [
            //Service
            'Notifier\Service\NotifierAgregator' => 'Notifier\Service\NotifierAgregator',
            'Notifier\Service\EmailNotifier'     => 'Notifier\Service\EmailNotifier',
            'Notifier\Service\PhoneNotifier'     => 'Notifier\Service\PhoneNotifier',
        
            //Mapper
            'Notifier\Mapper\NotificationPhoneLog' => 'Notifier\Mapper\NotificationPhoneLog',
            
            //Entity
            'Notifier\Entity\NotificationPhoneLog' => 'Notifier\Entity\NotificationPhoneLog'
            
        ],
        
        'factories' => [
            'Notifier\Mapper\NotificationPhoneLogMapper' => function (ServiceLocatorInterface $SL)
            {
                $mapper = $SL->get('Notifier\Mapper\NotificationPhoneLog');
                $mapper->setEntityPrototype($SL->get('Notifier\Entity\NotificationPhoneLog'));
                $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                $mapper->setHydrator(new ClassMethods());
                return $mapper;
            }
        ],
    ];
?>