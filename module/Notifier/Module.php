<?php
namespace Notifier;

class Module
{   
    public function getConfig()
    {
        return require __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return require __DIR__ . '/config/service.config.php';
    }
    
    public function getAutoloaderConfig()
    {
        return require __DIR__ . '/config/autoloader.config.php';
    }     
}

