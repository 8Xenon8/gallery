<?php

return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'controller_plugins' => array(
    ),

    'view_manager' => array(
        // 'layout'              => 'layout/gallery',
        'template_map' => array(
            'layout/gallery'           => __DIR__ . '/../view/layout/layout.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'router' => array(
        'routes' => array(

            'refine' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/refine/[:test]',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Index',
                        'action'     => 'refine',
                    ),
                ),
            ),

            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'gallery' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/gallery/[:tags]',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Index',
                        'action'     => 'gallery',
                    ),
                ),
                'may_terminate' => true
            ),

            'tags' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/tags/[:tags]',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Index',
                        'action'     => 'tags',
                    ),
                ),
                'may_terminate' => true
            ),

            'auth' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/auth',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Admin',
                        'action'     => 'auth',
                    ),
                ),
            ),

            'ressurect' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/ressurect',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Admin',
                        'action'     => 'ressurect',
                    ),
                ),
            ),

            'import' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/import',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Admin',
                        'action'     => 'import',
                    ),
                ),
            ),

            'admin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        // '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Gallery\Controller\Admin',
                        'action'     => 'admin',
                    ),
                ),
                'child_routes' => array(
                    'create' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => '/create',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'createGallery',
                            ),
                        ),
                    ),
                    'update' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/update/[:id]',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'updateGallery',
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/delete/[:id]',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'deleteGallery',
                            ),
                        ),
                    ),
                    'move' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/move',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'moveImage',
                            ),
                        ),
                    ),
                    'tags' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => '/tags',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'tags',
                            ),
                        ),
                        'child_routes' => array(
                            'addTag' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => '/add',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'addTag',
                                    ),
                                ),
                                'may_terminate' => true
                            ),
                            'deleteTag' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/delete/[:tagId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'deleteTag',
                                    ),
                                ),
                                'may_terminate' => true
                            ),
                        ),
                        'may_terminate' => true
                    ),
                    'gallery' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/gallery/[:id]',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'gallery',
                            ),
                        ),
                        'child_routes' => array(
                            'getImages' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => '/get',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'getImages',
                                    ),
                                ),
                            ),
                            'addImage' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => '/upload',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'uploadImage',
                                    ),
                                ),
                            ),
                            'editImage' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/edit/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'editImage',
                                    ),
                                ),
                            ),
                            'publish' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/publish/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'publish',
                                    ),
                                ),
                            ),
                            'unpublish' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/unpublish/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'unpublish',
                                    ),
                                ),
                            ),
                            'deleteImage' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/deleteImage/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'deleteImage',
                                    ),
                                ),
                            ),
                            'getImageTags' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/get-tags/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'getImageTags',
                                    ),
                                ),
                            ),
                            'saveImageTags' => array(
                                'type' => 'Zend\Mvc\Router\Http\Segment',
                                'options' => array(
                                    'route'    => '/save-tags/[:imageId]',
                                    'defaults' => array(
                                        // '__NAMESPACE__' => 'Gallery\Controller',
                                        'controller' => 'Gallery\Controller\Admin',
                                        'action'     => 'saveImageTags',
                                    ),
                                ),
                            ),
                        ),
                        'may_terminate' => true
                    ),
                    'changeImageOrder' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => '/change-image-order',
                            'defaults' => array(
                                // '__NAMESPACE__' => 'Gallery\Controller',
                                'controller' => 'Gallery\Controller\Admin',
                                'action'     => 'changeImageOrder',
                            ),
                        ),
                    ),
                ),
                'may_terminate' => true
            ),


        )
    ),
);