<?php
namespace Gallery\Mapper;

use Gallery\Mapper\AbstractDbMapperWithEntity;

class Image extends AbstractDbMapperWithEntity
{
    protected $tableName = 'image';
}

