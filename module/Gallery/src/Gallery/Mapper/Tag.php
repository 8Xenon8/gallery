<?php
namespace Gallery\Mapper;

use Gallery\Mapper\AbstractDbMapperWithEntity;

class Tag extends AbstractDbMapperWithEntity
{
    protected $tableName = 'tag';
}

