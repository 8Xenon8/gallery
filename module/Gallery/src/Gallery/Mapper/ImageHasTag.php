<?php
namespace Gallery\Mapper;

use Gallery\Mapper\AbstractDbMapperWithEntity;

class ImageHasTag extends AbstractDbMapperWithEntity
{
    protected $tableName = 'image_has_tag';
}

