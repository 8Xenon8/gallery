<?php
namespace Gallery\Mapper;

use Gallery\Mapper\AbstractDbMapperWithEntity;

class Gallery extends AbstractDbMapperWithEntity
{
    protected $tableName = 'gallery';
}

