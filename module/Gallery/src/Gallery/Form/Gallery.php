<?php
namespace Gallery\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class Gallery extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = []){
        parent::__construct('gallery');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'type' => 'text',
            'name' => 'name',
            'options' => array(
                'label' => 'Название'
            ),
            // 'attributes' => array(
            //     'class' => 'form-control',
            //     'id' => 'name',
            //     'required' => 'required'
            // ),
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'code',
            'options' => array(
                'label' => 'Код'
            ),
            // 'attributes' => array(
            //     'class' => 'form-control',
            //     'id' => 'phone',
            //     'required' => 'required'
            // ),
        ));

        $this->add(array(
            'type' => 'textarea',
            'name' => 'description',
            'options' => array(
                'label' => 'Описание'
            ),
            // 'attributes' => array(
            //     'class' => 'form-control',
            //     'id' => 'phone',
            //     'required' => 'required'
            // ),
        ));

        // $this->add(array(
        //     'type' => 'text',
        //     'name' => 'email',
        //     'options' => array(
        //         'label' => 'Email'
        //     ),
        //     'attributes' => array(
        //         'class' => 'form-control',
        //         'id' => 'email'
        //     ),
        // ));

        // $this->add(array(
        //     'type' => 'text',
        //     'name' => 'vk',
        //     'options' => array(
        //         'label' => 'ID ВКонтакте'
        //     ),
        //     'attributes' => array(
        //         'class' => 'form-control',
        //         'id' => 'vk'
        //     ),
        // ));

        // $this->add(array(
        //     'type' => 'select',
        //     'name' => 'day',
        //     'options' => array(
        //         'label' => 'Число *'
        //     ),
        //     'attributes' => array(
        //         'class' => 'form-control',
        //         'id' => 'day',
        //         'required' => 'required'
        //     ),
        // ));

        // $this->add(array(
        //     'type' => 'select',
        //     'name' => 'time',
        //     'options' => array(
        //         'label' => 'Время *',
        //         'label_attributes' => array(
        //             'id' => 'time-label'
        //         )
        //     ),
        //     'attributes' => array(
        //         'class' => 'form-control',
        //         'id' => 'time',
        //         'required' => 'required'
        //     ),
        // ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => array(
                'value' => 'Сохранить',
                'class' => 'btn btn-info'
            ),
        ));

    }

    public function getInputFilterSpecification()
    {
        return array(

            'code' => array(
                'required' => true,
                'filters'  => array(
                    array(
                        'name' => 'Zend\Filter\StringTrim'
                    ),
                    array(
                        'name' => 'Zend\Filter\StripTags'
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\StringLength',
                        'options' => array(
                            'min' => 3,
                            'message' => array(
                                \Zend\Validator\StringLength::TOO_SHORT => "Слишком короткий код"
                            )
                        )
                        
                    ),
                    array(
                        'name' => 'Zend\Validator\Regex',
                        'options' => array(
                            'pattern' => '/[A-Za-z0-9]+/',
                            'message' => array(
                                \Zend\Validator\Regex::INVALID => "Недопустимые символы"
                            )
                        )
                    )
                )
            ),

            // ),

            'name' => array(
                'required' => true,
                'filters'  => array(
                    array(
                        'name' => 'Zend\Filter\StringTrim'
                    ),
                    array(
                        'name' => 'Zend\Filter\StripTags'
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\StringLength',
                        'options' => array(
                            'min' => 3,
                            'message' => array(
                                \Zend\Validator\StringLength::TOO_SHORT => "Слишком короткое название"
                            )
                        )  
                    )
                )

            ),

            // 'email' => array(
            //     'required' => false,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),

            // ),

            // 'vk' => array(
            //     'required' => false,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),

            // ),

            // 'day' => array(
            //     'required' => true,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),

            // ),

            // 'time' => array(
            //     'required' => true,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),

            // ),

            // 'title' => array(
            //     'required' => true,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),
                
            //     'validators' => array(
            //         array(
            //             'name' => 'Zend\Validator\StringLength',
            //             'options' => array(
            //                 'min' => 3
            //             ),
            //         ),
                    
            //     ),
            // ),

            // 'description' => array(
            //     'required' => false,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),
            // ),

            // 'base_price' => array(
            //     'required' => true,
            //     'filters'  => array(
            //         array(
            //             'name' => 'Zend\Filter\StringTrim'
            //         ),
            //         array(
            //             'name' => 'Zend\Filter\StripTags'
            //         ),
            //     ),
            // ),

            // 'image' => array(

            //     'required' => false,
            //     'validators' => array(
            //         array(
            //             'name' => 'Zend\Validator\File\Size',
            //             'options' => array(
            //                 'max' => "5MB"
            //             ),
            //         ),

            //         array(
            //             'name' => 'Zend\Validator\File\IsImage',
            //         ),

            //          array(
            //              'name' => 'Zend\Validator\File\Extension',
            //              'options' => array(
            //                  'extension' => "jpg,png,gif",
            //              ),
            //          ),
            //     ),
            //     'filters' => array(
            //         array(
            //             'name' => 'Zend\Filter\File\RenameUpload',
            //             'options' => array(
            //                 'target' => 'data\tmp\\',
            //                 'randomize' => true,
            //                 'use_upload_extension' => true
            //             )
            //         )
            //     )
            // )
        );
    }
}

?>