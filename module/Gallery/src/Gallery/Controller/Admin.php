<?php
namespace Gallery\Controller;

use Zend\View\Model\JsonModel;

class Admin extends \Zend\Mvc\Controller\AbstractActionController
{

	public function authAction()
    {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') == 'true') {
            return $this->redirect()->toRoute('home');
        };

        if ($this->request->isPost()) {
            if ($this->params()->fromPost()['password'] == 'letmypeO69') {
                // die('correct!');
                $session->offsetSet('auth', 'true');
                return $this->redirect()->toRoute('admin');
            }

            return $this->redirect()->toRoute('home');
        }

    }

    public function adminAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $galleries = $this->getServiceLocator()->get('Gallery/Mapper/GalleryMapper')->getEntityList()->toArray();

        return [
            'galleries' => $galleries
        ];

    }

    public function galleryAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        
    	$galleryId = $this->params()->fromRoute('id');

        if (!$this->getServiceLocator()->get('Gallery\Mapper\GalleryMapper')->getEntityListByFilter(['id' => $galleryId])->current()) { return $this->getResponse()->setStatusCode(404); }

    	$images = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper')->getEntityListByFilter(['gallery_id' => $galleryId]);

        $tags = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityList()->toArray();

        $galleries = $this->getServiceLocator()->get('Gallery\Mapper\GalleryMapper')->getEntityListByFilter([
            (new \Zend\Db\Sql\Where())->notEqualTo('id', $galleryId)])->toArray();

    	return [
    		'images' => $images,
            'tags' => $tags,
            'galleries' => $galleries
    	];

    }

    public function getImagesAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $galleryId = $this->params()->fromRoute('id');

        $images = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper')->getEntityListByFilter(['gallery_id' => $galleryId]);

        return $response->setContent(\Zend\Json\Json::encode([
            'images' => $images
        ]));
    }

    public function createGalleryAction()
    {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        
        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\GalleryMapper');
        $form = $this->getServiceLocator()->get('Gallery\Form\GalleryForm');

        $prg = $this->fileprg($form);

        if ($prg instanceof Response){

            return $prg;

        } elseif (is_array($prg)) {

            if ($form->isValid()) {

                $data = $form->getData();

                try {
                    $mapper->insert([
                        'name' => $data['name'],
                        'code' => $data['code'],
                        'description' => $data['description']
                    ]);
                    $this->flashMessenger()->addSuccessMessage("Запись обновлена!");
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('При обработке данных произошла ошибка. Повторите позднее');
                    // $this->flashMessenger()->addErrorMessage($e->getMessage());
                }

                return $this->redirect()->toRoute('admin');

            };

        }

        return [
            'form' => $form
        ];
    }

    public function updateGalleryAction()
    {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        
        $id = $this->params()->fromRoute('id');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\GalleryMapper');
        $form = $this->getServiceLocator()->get('Gallery\Form\GalleryForm');

        $entity = $mapper->getEntityListByFilter(['id' => $id])->current();

        $prototype = $mapper->getEntityPrototype();

        $form->get('name')->setValue($entity->getName());
        $form->get('code')->setValue($entity->getCode());
        $form->get('description')->setValue($entity->getDescription());

        $prg = $this->fileprg($form);

        if ($prg instanceof Response){

            return $prg;

        } elseif (is_array($prg)) {

            if ($form->isValid()) {

                $data = $form->getData();

                try {
                    $mapper->update(
                        [
                            'name' => $data['name'],
                            'code' => $data['code'],
                            'description' => $data['description']
                        ],
                        [
                            'id' => $id
                        ]
                    );
                    $this->flashMessenger()->addSuccessMessage("Галерея обновлена!");
                    return $this->redirect()->toRoute('admin');

                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage('При обработке данных произошла ошибка. Повторите позднее');
                    $this->redirect()->refresh();
                }

                

            };

        }

        return [
            'form' => $form
        ];
    }

    public function deleteGalleryAction()
    {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        

        // die('123');
        $id = $this->params()->fromRoute('id');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\GalleryMapper');
        $imageMapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');
        $images = $imageMapper->getEntityListByFilter(['gallery_id' => $id])->toArray();

        // var_dump($id); die;

        foreach ($images as $key => $image) {

            $path = getcwd() . '/public/img/upload/';

            try {
                unlink($path . $image['orig']);
            } catch(\Exception $error) {
                $this->flashMessenger()->addErrorMessage('Не удалось удалить файл' . $image['orig']);
            }

            try {
                unlink($path . $image['preview']);
            } catch(\Exception $error) {
                $this->flashMessenger()->addErrorMessage('Не удалось удалить файл' . $image['preview']);
            }

            try {
                unlink($path . $image['thumb']);
            } catch(\Exception $error) {
                $this->flashMessenger()->addErrorMessage('Не удалось удалить файл' . $image['thumb']);
            }
        }

        // if (!$imageMapper->delete(['id' => $imageIds])) {
        //     $this->flashMessenger()->addErrorMessage("Ошибка при удалении картинок из БД. Попробуйте еще раз");
        // }

        try {
            $mapper->delete(['id' => $id]);
        } catch(\Exception $error) {
            $this->flashMessenger()->addErrorMessage("Ошибка при удалении галереи из БД. Попробуйте еще раз");
        }

        // try {
    	$this->flashMessenger()->addSuccessMessage("Галерея удалена!");        
        $this->redirect()->toRoute('admin');
    }

    public function uploadImageAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            // return $this->redirect()->toRoute('auth');
        };
        

    	$galleryId = $this->params()->fromRoute('id');
        $imageMapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

    	$form = $this->getServiceLocator()->get('Gallery\Form\UploadForm');

    	$request = $this->getRequest();
   		
        if (!$request->isPost()) {
            return $this->redirect()->toRoute('admin');
        }

   		if ($file = $this->getRequest()->getFiles()['file']) {

   			$form->setData([
   				'file' => $file
   			]);

        	if (!$form->isValid()) {
                return $this->getResponse()->setStatusCode(406);
            };

        	$data = $form->getData();

            $image = $this->getServiceLocator()->get('Gallery\Model\ImageModel')->processImage($data['file']['tmp_name']);

            $data = [];

            try {

                $insert = $imageMapper->insert([
                    'gallery_id' => $galleryId,
                    'orig' => $image['original_src'],
                    'preview' => $image['preview_src'],
                    'thumb' => $image['thumbnail_src'],
                    'orig_width' => $image['size']['orig'][0],
                    'orig_height' => $image['size']['orig'][1],
                    'preview_width' => $image['size']['preview'][0],
                    'preview_height' => $image['size']['preview'][1],
                    'thumb_width' => $image['size']['thumb'][0],
                    'thumb_height' => $image['size']['thumb'][1]
                ]);

                $data[] = [
                    'id' => $insert->getGeneratedValue(),
                    'orig' => $image['original_src'],
                    'preview' => $image['preview_src'],
                    'thumb' => $image['thumbnail_src'],
                    'size' => $image['size']
                ]; 

            } catch(\Exception $error) {

                return $this->getResponse()->setStatusCode(500);

            }

            // }

            $response = $this->getResponse();
            return $response->setContent(\Zend\Json\Json::encode($data));
    	}

    }

    public function editImageAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        

        $imageId = $this->params()->fromRoute('imageId');
        $galleryId = $this->params()->fromRoute('id');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

        if ($this->getRequest()->isPost()) {

            $post = $this->params()->fromPost();

            // var_dump($imageId); die;

            if (isset($post['publish'])) {
                $post['publish'] = (bool)$post['publish'];
            }
 
            try {

                $mapper->update(
                    $post,
                    [
                        'id' => $imageId
                    ]
                );

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => true
                ]));

            } catch(\Exception $error) {

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => false,
                    'error' => $error->getMessage()
                ]));
                
            }

        }

    }

    public function publishAction() {

        $session = new \Zend\Session\Container('auth');
        
        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $imageId = $this->params()->fromRoute('imageId');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');
        $response = $this->getResponse();

        if ($this->getRequest()->isPost()) {

            $post = $this->params()->fromPost();

            // var_dump($imageId); die;

            if (isset($post['publish'])) {
                $post['publish'] = (bool)$post['publish'];
            }
 
            try {

                $mapper->update(
                    [
                        'publish' => true
                    ],
                    [
                        'id' => $imageId
                    ]
                );

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => true
                ]));

            } catch(\Exception $error) {

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => false,
                    'error' => $error->getMessage()
                ]));
                
            }

        }

    }

    public function unpublishAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        

        $imageId = $this->params()->fromRoute('imageId');
        $galleryId = $this->params()->fromRoute('id');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

        if ($this->getRequest()->isPost()) {

            $post = $this->params()->fromPost();

            // var_dump($imageId); die;

            if (isset($post['publish'])) {
                $post['publish'] = (bool)$post['publish'];
            }
 
            try {

                $mapper->update(
                    [
                        'publish' => false
                    ],
                    [
                        'id' => $imageId
                    ]
                );

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => true
                ]));

            } catch(\Exception $error) {

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => false,
                    'error' => $error->getMessage()
                ]));
                
            }

        }

    }

    public function moveImageAction()
    {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        

        // $imageId = $this->params()->fromRoute('imageId');
        // $galleryId = $this->params()->fromRoute('id');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

        if ($this->getRequest()->isPost()) {

            $post = $this->params()->fromPost();

            $imageId = $post['image_id'];
            $galleryId = $post['gallery_id'];
 
            try {

                $mapper->update(
                    [
                        'gallery_id' => $galleryId
                    ],
                    [
                        'id' => $imageId
                    ]
                );

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => true
                ]));

            } catch(\Exception $error) {

                return $response->setContent(\Zend\Json\Json::encode([
                    'success' => false,
                    'error' => $error->getMessage()
                ]));
                
            }

        }

    }

    public function deleteImageAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        
        $id = $this->params()->fromRoute('imageId');

        $model = $this->getServiceLocator()->get('Gallery\Model\ImageModel');

        if ($model->deleteImage($id)) {

            return $response->setContent(\Zend\Json\Json::encode([
                'success' => true
            ]));

        } else {

            return $response->setContent(\Zend\Json\Json::encode([
                'success' => false,
                'error' => $model->getLastError()
            ]));

        } 
    }


    public function getImageTagsAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };
        
        $id = $this->params()->fromRoute('imageId');

        $model = $this->getServiceLocator()->get('Gallery\Model\ImageModel');
        $tags = $model->getImageTags($id);
        $response = $this->getResponse();

        return $response->setContent(\Zend\Json\Json::encode([
            'tags' => $tags
        ]));
    }


    public function saveImageTagsAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $ids = explode(',', $this->params()->fromRoute('imageId'));
        $request = $this->getRequest();
        $response = $this->getResponse();

        $errors = [];

        if (!$request->isPost()) { return $this->getResponse()->setStatusCode(406); }

        $data = $this->params()->fromPost();
        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageHasTagMapper');

        foreach ($ids as $id) {

            try {
                $mapper->delete([
                    'image_id' => $id,
                    'tag_id' => $data
                ]);

                foreach ($data as $tag) {
                    $mapper->insert([
                        'image_id' => $id,
                        'tag_id' => $tag
                    ]);
                }

            } catch(\Exception $error) {


                $errors[] = $error->getMessage();
                // return $response->setContent(\Zend\Json\Json::encode([
                //     'success' => false,
                //     'error' => $error->getMessage()
                // ]));

            }

        }

        return $response->setContent(\Zend\Json\Json::encode([
            'success' => true,
            'errors' => $errors
        ]));

    }

    public function tagsAction()
    {
        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $tags = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityList()->toArray();

        return [
            'tags' => $tags
        ];
    }

    public function addTagAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        if (!$this->getRequest()->isPost()) { return $this->getResponse()->setStatusCode(404); }

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper');
        $name = $this->params()->fromPost()['name'];
        $response = $this->getResponse();

        $existingName = $mapper->getEntityListByFilter([
            'name' => $name
        ])->current();

        if ($existingName) {
            return $response->setContent(\Zend\Json\Json::encode([
                'success' => false,
                'error' => "Имя уже существует"
            ]));

        }

        try {
            $insert = $mapper->insert([
                'name' => $name
            ]);

            return $response->setContent(\Zend\Json\Json::encode([
                'success' => true,
                'id' => $insert->getGeneratedValue()
            ]));
        } catch (\Error $error) {
            return $response->setContent(\Zend\Json\Json::encode([
                'success' => false,
                'error' => $error->getMessage()
            ]));
        }
    }

    public function deleteTagAction() {

        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        if (!$this->getRequest()->isPost()) { return $this->getResponse()->setStatusCode(404); }

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper');
        $id = $this->params()->fromRoute()['tagId'];
        $response = $this->getResponse();

        try {
            $mapper->delete([
                'id' => $id
            ]);

            return $response->setContent(\Zend\Json\Json::encode([
                'success' => true
            ]));
        } catch (\Error $error) {
            return $response->setContent(\Zend\Json\Json::encode([
                'success' => false,
                'error' => $error->getMessage()
            ]));
        }
    }

    public function importAction() {        
        $imageMapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

        $request = $this->getRequest();
        
        // if (!$request->isPost()) {
        //     return $this->redirect()->toRoute('admin');
        // }

        if ($file = $this->getRequest()->getFiles()['file']) {

            $image = $this->getServiceLocator()->get('Gallery\Model\ImageModel')->processImage($_FILES['file']['tmp_name']);

            try {

                $insert = $imageMapper->insert([
                    'gallery_id' => 42,
                    'orig' => $image['original_src'],
                    'preview' => $image['preview_src'],
                    'thumb' => $image['thumbnail_src'],
                    'orig_width' => $image['size']['orig'][0],
                    'orig_height' => $image['size']['orig'][1],
                    'preview_width' => $image['size']['preview'][0],
                    'preview_height' => $image['size']['preview'][1],
                    'thumb_width' => $image['size']['thumb'][0],
                    'thumb_height' => $image['size']['thumb'][1],
                    'name' => $this->params()->fromPost('name'),
                    'description' => $this->params()->fromPost('description'),
                ]);

            } catch(\Exception $error) {

                echo $error->getMessage();
                return $this->getResponse()->setStatusCode(500);

            }
            
        } else {
            echo 'fail';
        }

        die;

    }

    public function changeImageOrderAction() {
        $item = $this->params()->fromPost('item');
        $target = $this->params()->fromPost('target');

        if ($item === null || $target === null) {
            return $this->getResponse()->setStatusCode(500);
        }

        $model = $this->getServiceLocator()->get('Gallery\Model\ImageModel');

        if (!$model->changeImageOrder($item, $target)) {
            die($model->getLastError());
        }

        return $this->getResponse()->setStatusCode(200);
    }

    public function ressurectAction()
    {
        $files = scandir(__DIR__ . '/../../../../../public/img/upload');

        $mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');

        foreach ($files as $key => $file) {
            if ($file == '.' || $file == '..') { continue; }
            $filename = __DIR__ . '/../../../../../public/img/upload/'.$file;
            if (file_exists($filename)) {
                $filesize = (getimagesize($filename));

                $max = max([$filesize[0], $filesize[1]]);

                // if ($max) > 0

                switch (substr($file, 0, 3)) {
                    case 'pr_':
                        $image = $mapper->getEntityListByFilter([
                            'preview_width' => $filesize[0],
                            'preview_height' => $filesize[1]
                        ])->toArray();
                        break;
                    case 'tn_':
                        continue;
                        $image = $mapper->getEntityListByFilter([
                            'thumb_width' => $filesize[0],
                            'thumb_height' => $filesize[1]
                        ])->toArray();
                        break;
                    default:
                        continue;
                        $image = $mapper->getEntityListByFilter([
                            'orig_width' => $filesize[0],
                            'orig_height' => $filesize[1]
                        ])->toArray();
                        break;
                }

                if (count($image) == 0) {
                    echo $filesize[0] . '; ';
                    echo $filesize[1];
                    echo '<br />';
                }
            }
        }
    }

}