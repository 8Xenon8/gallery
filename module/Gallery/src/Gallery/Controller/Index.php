<?php
namespace Gallery\Controller;

class Index extends \Zend\Mvc\Controller\AbstractActionController
{

    public function indexAction()
    {

        $imageMapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');
        $images = $imageMapper->getEntityListByFilter([
            'publish' => 1
        ])->toArray();

        $relations = $this->getServiceLocator()->get('Gallery\Mapper\ImageHasTagMapper')->getEntityList()->toArray();
        $relationIds = array_map(function($i) { return $i['tag_id']; }, $relations);

        if (count($relationIds) > 0) {

            $tags = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityListByFilter(['id' => $relationIds])->toArray();
            $tagIds = array_map(function($i) { return $i['id']; }, $tags);
            $tags = array_combine($tagIds, $tags);

        } else {
            $tags = [];
        }

        foreach ($images as $key => $image) {

            $images[$key]['tags'] = [];

            foreach ($relations as $rel) {
                if ($rel['image_id'] == $image['id']) {
                    $images[$key]['tags'][] = $tags[$rel['tag_id']];
                }
            }
        }

        return [
            'images' => $images,
            'tags' => $tags
        ];
    }

    public function galleryAction()
    {
        $tags = explode(',', $this->params()->fromRoute('tags'));
        $tagIds = array_map(
            function($i) {
                return $i['id'];
            },
            $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityListByFilter(['name' => $tags])->toArray()
        );

        if (empty($tagIds)) {      
            return $this->getResponse()->setStatusCode(404);
        } 
        
        $imageIds = array_map(
            function($i) {
                return $i['image_id'];
            },
            $this->getServiceLocator()->get('Gallery\Mapper\ImageHasTagMapper')->getEntityListByFilter(['tag_id' => $tagIds])->toArray()
        );

        $imageMapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');
        $images = $imageMapper->getEntityListByFilter([
            'id' => $imageIds,
            'publish' => true
        ])->toArray();

        if (!count($images)) {

            return $this->getResponse()->setStatusCode(404);

            $imageIds = array_map(function($i) { return $i['id']; }, $images);

            $relations = $this->getServiceLocator()->get('Gallery\Mapper\ImageHasTagMapper')->getEntityListByFilter([
                'image_id' => $imageIds
            ])->toArray();
        } else {
            $relations = [];
        }

        if (count($relations) > 0) {

            $tagIds = array_map(function($i) { return $i['tag_id']; }, $relations);

            $tags = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityListByFilter([
                'id' => $tagIds
            ])->toArray();
            $tagIds = array_map(function($i) { return $i['id']; }, $tags);

            $tags = array_combine($tagIds, $tags);

        } else {
            $tags = [];
        }


        foreach ($images as $key => $image) {

            $images[$key]['tags'] = [];

            foreach ($relations as $rel) {
                if ($rel['image_id'] == $image['id']) {
                    $images[$key]['tags'][] = $tags[$rel['tag_id']];
                }
            }
        }

        return [
            'images' => $images,
            'tags' => $tags
        ];

    }

    

    // public function updateGalleryAction()
    // {

    //     $session = new \Zend\Session\Container('auth');

    //     if ($session->offsetGet('auth') != 'true') {
    //         return $this->redirect()->toRoute('auth');
    //     };

    //     $id = $this->params()->fromRoute('id');

    //     $form = $this->getServiceLocator()->get('Registration\Form\RegistrationForm');
    //     $dayMapper = $this->getServiceLocator()->get('Registration\Mapper\DayMapper');
    //     $timeMapper = $this->getServiceLocator()->get('Registration\Mapper\TimeMapper');
    //     $entryMapper = $this->getServiceLocator()->get('Registration\Mapper\EntryMapper');
    //     $entryEntity = $entryMapper->getEntityListByFilter([
    //         'id' => $id
    //     ])->current();

    //     if (!$entryEntity) {
    //         return $this->getResponse()->SetStatusCode(404);
    //     }

    //     $form->get('name')->setValue($entryEntity->getName());
    //     $form->get('phone')->setValue($entryEntity->getPhone());
    //     $form->get('email')->setValue($entryEntity->getEmail());
    //     $form->get('vk')->setValue($entryEntity->getVk());
    //     $form->get('day')->setValue($entryEntity->getDay());
    //     $form->get('time')->setValue($entryEntity->getTime());
    //     $form->get('submit')->setValue("Сохранить");

    //     $prg = $this->fileprg($form);

    //     $entries = $this->getServiceLocator()->get('Registration\Mapper\EntryMapper')->getEntityList()->toArray();

    //     if($prg instanceof Response){

    //         return $prg;

    //     } elseif (is_array($prg)) {

    //         if ($form->isValid()) {

    //             if ($entryMapper->getEntityListByFilter([
    //                 'time' => $prg['time'],
    //                 'day' => $prg['day']
    //             ])->current()) {
    //                 $this->flashMessenger()->addErrorMessage("Кто-то уже записался на это время!");
    //             } else {

    //                 try {
    //                     $entryMapper->update([
    //                         'name' => $prg['name'],
    //                         'email' => $prg['email'],
    //                         'day' => $prg['day'],
    //                         'time' => $prg['time'],
    //                     ],
    //                     ['id' => $id]);
    //                     $this->flashMessenger()->addSuccessMessage("Запись обновлена!");
    //                 } catch (\Exception $e) {
    //                     $this->flashMessenger()->addErrorMessage('При обработке данных произошла ошибка. Повторите позднее');
    //                 }
    //             };

    //             return $this->redirect()->refresh();

    //         };

    //     }

    //     $occupied = [];

    //     foreach ($entries as $entry) {
    //         if (!isset($occupied[$entry['day']])) {
    //             $occupied[$entry['day']] = [];
    //         }

    //         $occupied[$entry['day']][] = $entry['time'];
    //     }

    //     return [
    //         'form' => $form,
    //         'occupied' => $occupied
    //     ];
    // }

    public function deleteAction()
    {
        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $id = $this->params()->fromRoute('id');

        $entryMapper = $this->getServiceLocator()->get('Registration\Mapper\EntryMapper');
        try {
            $entryMapper->delete([
                'id' => $id
            ]);
            $this->flashMessenger()->addSuccessMessage("Запись удалена");
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage($e->getMessage());
        }
        return $this->redirect()->toRoute('admin');
    }



    public function addBlockAction()
    {
        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $day = rawurldecode($this->params()->fromRoute('day'));
        $time = rawurldecode($this->params()->fromRoute('time'));

        try {
            $this->getServiceLocator()->get('Registration\Mapper\EntryMapper')->insert([
                'disabled' => 1,
                'day' => $day,
                'time' => $time,
            ]);
            $this->flashMessenger()->addSuccessMessage("Запись заблокирована");
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('При обработке данных произошла ошибка. Повторите позднее');
        }

        return $this->redirect()->toRoute('admin');
    }

    public function approveAction()
    {
        $session = new \Zend\Session\Container('auth');

        if ($session->offsetGet('auth') != 'true') {
            return $this->redirect()->toRoute('auth');
        };

        $id = $this->params()->fromRoute('id');

        try {
            $this->getServiceLocator()->get('Registration\Mapper\EntryMapper')->update([
                'approved' => 1,
            ],
            [
                'id' => $id
            ]);
            $this->flashMessenger()->addSuccessMessage("Запись одобрена");
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('При обработке данных произошла ошибка. Повторите позднее');
        }

        return $this->redirect()->toRoute('admin');

    }

    public function TagsAction()
    {
        $selectedTags = explode(',', $this->params()->fromRoute('tags'));
        $allTagEntities = $this->getServiceLocator()->get('Gallery\Mapper\TagMapper')->getEntityList()->toArray();

        $allTagsNames = array_map(function($i) { return $i['name']; }, $allTagEntities);

        $tagRelations = $this->getServiceLocator()->get('Gallery\Mapper\ImageHasTagMapper')->getEntityList()->toArray();
        $imageIds = array_map(function($i) { return $i['image_id']; }, $tagRelations);

        if (count($imageIds) > 0) {

            $images = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper')->getEntityListByFilter([
                'id' => $imageIds
            ])->toArray();

        } else {
            $images = [];
        }

        return [
            'allTags' => $allTagsNames,
            'selectedTags' => $selectedTags,
        ];
    }

}