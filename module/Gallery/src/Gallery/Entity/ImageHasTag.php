<?php
namespace Gallery\Entity;

class ImageHasTag
{
    protected
        $tagId,
        $imageId;

    public function getTagId()
    {
        return $this->tagId;
    }

    public function setTagId($tagId)
    {
        $this->tagId = $tagId;
    }


    public function getImageId()
    {
        return $this->imageId;
    }

    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    
}