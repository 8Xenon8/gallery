<?php
namespace Gallery\Entity;

class Upload
{
    protected
        $id,
        $galleryId,
        $orig,
        $preview,
        $thumb;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getGalleryId()
    {
        return $this->galleryId;
    }

    public function setGalleryId($galleryId)
    {
        $this->galleryId = $galleryId;
    }


    public function getOrig()
    {
        return $this->orig;
    }

    public function setOrig($orig)
    {
        $this->orig = $orig;
    }


    public function getPreview()
    {
        return $this->preview;
    }

    public function setPreview($preview)
    {
        $this->preview = $preview;
    }


    public function getThumb()
    {
        return $this->thumb;
    }

    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
    }
    
}