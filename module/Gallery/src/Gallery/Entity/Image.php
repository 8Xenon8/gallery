<?php
namespace Gallery\Entity;

class Image
{
    protected
        $id,
        $name,
        $description,
        $galleryId,
        $orig,
        $preview,
        $thumb,
        $origWidth,
        $origHeight,
        $previewWidth,
        $previewHeight,
        $thumbWidth,
        $thumbHeight,
        $publish,
        $order;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function getGalleryId()
    {
        return $this->galleryId;
    }

    public function setGalleryId($galleryId)
    {
        $this->galleryId = $galleryId;
    }


    public function getOrig()
    {
        return $this->orig;
    }

    public function setOrig($orig)
    {
        $this->orig = $orig;
    }


    public function getPreview()
    {
        return $this->preview;
    }

    public function setPreview($preview)
    {
        $this->preview = $preview;
    }


    public function getThumb()
    {
        return $this->thumb;
    }

    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
    }


    public function getOrigWidth()
    {
        return $this->origWidth;
    }

    public function setOrigWidth($origWidth)
    {
        $this->origWidth = $origWidth;
    }


    public function getOrigHeight()
    {
        return $this->origHeight;
    }

    public function setOrigHeight($origHeight)
    {
        $this->origHeight = $origHeight;
    }


    public function getPreviewWidth()
    {
        return $this->previewWidth;
    }

    public function setPreviewWidth($previewWidth)
    {
        $this->previewWidth = $previewWidth;
    }


    public function getPreviewHeight()
    {
        return $this->previewHeight;
    }

    public function setPreviewHeight($previewHeight)
    {
        $this->previewHeight = $previewHeight;
    }


    public function getThumbWidth()
    {
        return $this->thumbWidth;
    }

    public function setThumbWidth($thumbWidth)
    {
        $this->thumbWidth = $thumbWidth;
    }


    public function getThumbHeight()
    {
        return $this->thumbHeight;
    }

    public function setThumbHeight($thumbHeight)
    {
        $this->thumbHeight = $thumbHeight;
    }


    public function getPublish()
    {
        return $this->publish;
    }

    public function setPublish($publish)
    {
        $this->publish = $publish;
    }


    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }
    
}