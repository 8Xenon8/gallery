<?php
namespace Shop\Entity\Hydrator;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Hydrator\Aggregate\AggregateHydrator;

class Response implements HydratorInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    
    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object) {

        $arr = [];
        
        if ($object instanceof Shop\Entity\Response) {

            $arr['id']       = $object->getId();
            $arr['user_id']  = $object->getUserId();
            $arr['request']    = $object->getRequest();
            $arr['products'] = $object->getProducts();

        }
        
        return $arr;
    }
    
    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array $data
     * @param  object $object
     * @return object
     */
    public function hydrate(array $data, $object) {
        
        if ($object instanceof Shop\Entity\Response) {

            if (isset($data["id"])) {
                $object->setId($data['id']);
            };
            if (isset($data["user_id"])) {
                $object->setId($data['user_id']);
            };

            // if (isset($data["products"])) {
                
            //     for

            //     $object->setProducts($data["products"];
            // } else {
            //     $object->setProducts([]);
            // }


            return $object;
        }
        
    }
   
}

?>