<?php

namespace Shop\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ProductFilter extends AbstractHelper
{
    public function __invoke()
    {
        return $this;
    }

    public function displayFilters($filters)
    {
        $form = new \Zend\Form\Form('filter');

        foreach ($filters as $name => $param) {

            foreach ($param['value'] as $value) {

                $el = new \Zend\Form\Element\Checkbox($name.'[value]['.$value.']');
                $el->setLabel($value);

                if (isset($filters[$name]) && isset($filters[$name]['checked']['value']) && in_array($value, $filters[$name]['checked']['value'])) {
                    $el->setChecked('true');
                }

                $form->add($el);
            }

            foreach ($param['type'] as $type) {

                $el = new \Zend\Form\Element\Checkbox($name.'[type]['.$type.']');
                $el->setLabel($type);

                if (isset($filters[$name]) && isset($filters[$name]['checked']['type']) && in_array($type, $filters[$name]['checked']['type'])) {
                    $el->setChecked('true');
                }

                $form->add($el);

            }

        }

        $form->setAttribute('method', 'GET');
        $form->add(new \Zend\Form\Element\Submit('submit'));

        foreach ($filters as $name => $filter) :
            if (isset($filter['value'])) : ?>

                <div class="dropdown_filter <?php if (isset($filter['checked'])) : echo 'active'; endif; ?>">
                    <?= $name ?>

                    <?php if (isset($filter['value'])) : ?>
                        <?php foreach ($filter['value'] as $value) : ?>

                            

                        <?php endforeach; ?>
                    <?php endif; ?>



                </div>

            <?php endif;
        endforeach;

    }

}



