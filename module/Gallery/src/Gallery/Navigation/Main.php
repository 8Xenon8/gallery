<?php
namespace Shop\Navigation;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Navigation\Service\DefaultNavigationFactory;

class Main extends DefaultNavigationFactory
{
    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if (null === $this->pages) {
            //FETCH data from table menu :
            $cats = $serviceLocator->get('Admin\Model\Category')->getNavArray();

            $name = $this->getName();

//            $nav = $serviceLocator->get('config')['navigation'];
//            $nav['default'] = array_map(function($i) use ($cats) {
//                if ($i['route'] == 'catalog') {
//                    $i['pages'] = $cats;
//                }
//                return $i;
//            }, $nav['default']);


            $configuration['navigation'][$name] = $cats;


            if (!isset($configuration['navigation'])) {
                throw new Exception\InvalidArgumentException('Could not find navigation configuration key');
            }
            if (!isset($configuration['navigation'][$this->getName()])) {
                throw new Exception\InvalidArgumentException(sprintf(
                'Failed to find a navigation container by the name "%s"',
                $name
                ));
            }

            $application = $serviceLocator->get('Application');
            $routeMatch  = $application->getMvcEvent()->getRouteMatch();
            $router      = $application->getMvcEvent()->getRouter();
            $pages       = $this->getPagesFromConfig($configuration['navigation'][$this->getName()]);

            $this->pages = $this->injectComponents($pages, $routeMatch, $router);
        }
        return $this->pages;
    }
}