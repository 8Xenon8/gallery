<?php
namespace Gallery\Model;

use \Zend\ServiceManager\ServiceLocatorAwareTrait;

class Image implements \Zend\ServiceManager\ServiceLocatorAwareInterface {

	use ServiceLocatorAwareTrait;

	public $lastError,
           $dbAdapter;

    public function processImage($name)
    {

        $dir  = getcwd();

        $path = $dir . $this->getServiceLocator()->get('config')['uploads']['path'];

        // лепим ватермарку
        // Webino watermark по каким-то причинам не хочет работать
        $wm = imagecreatefrompng($dir . '\public\img\watermark.png');
        $im = imagecreatefromjpeg($name);

        $tx = imagesx($wm);
        $ty = imagesy($wm);

        $ix = imagesx($im);
        $iy = imagesy($im);

        $ratio = $tx / ($ix / 4);

        imagecopyresampled($im, $wm, 0, 0, 0, 0, $tx / $ratio, $ty / $ratio, $tx, $ty);

        $ext  = '.jpg';

        // получаем хэш картинки и формируем путь, куда сохранять готовые картинки
        $newname  = md5_file($name);
        $newname  = md5($newname.time());
        $newname .= $ext;
        $fullpath = $dir.$newname;

        $thumbnailer = $this->getServiceLocator()->get('WebinoImageThumb');

        $thumb       = $thumbnailer->create($name);
        $origSize    = $thumb->getCurrentDimensions();

        // уменьшаем исходную картинку, если она слишком велика
        if ($max = max($origSize['width'], $origSize['height']) > 1000) {
            $ratio = 1000 / $max;

            $thumb->resize(1000, 1000);
        }

        $thumb->save($path.$newname);

        // делаем превью
        $thumb->resize(9999999, 300);
        $previewSize = $thumb->getCurrentDimensions();
        $thumb->save($path.'pr_'.$newname);

        // делаем малое превью
        $thumb->resize(9999999, 150);
        $thumbSize = $thumb->getCurrentDimensions();
        $thumb->save($path.'tn_'.$newname);

        return [
            'original_src'  => $newname,
            'preview_src'   => 'pr_'.$newname,
            'thumbnail_src' => 'tn_'.$newname,
            'tmp_name'      => $path.$newname,
            'size'          => [
                'orig'      => [
                    $origSize['width'],
                    $origSize['height']
                ],
                'preview'   => [
                    $previewSize['width'],
                    $previewSize['height']
                ],
                'thumb'     => [
                    $thumbSize['width'],
                    $thumbSize['height']
                ]
            ]
        ];
    }

    public function deleteImage($id) {

    	$mapper = $this->getServiceLocator()->get('Gallery\Mapper\ImageMapper');
        $image = $mapper->getEntityListByFilter(['id' => $id])->current();
        $path = getcwd() . $this->getServiceLocator()->get('config')['uploads']['path'];

        if (!$image) { return false; }

        try {
            if (file_exists($path.$image->getOrig())) {
                unlink($path.$image->getOrig());
            }
            if (file_exists($path.$image->getPreview())) {
                unlink($path.$image->getPreview());
            }
            if (file_exists($path.$image->getThumb())) {
                unlink($path.$image->getThumb());
            }

            try {
        	   $mapper->delete(['id' => $id]);
            } catch(\Exception $error) {
                $this->setLastError($error->getMessage());
                return false;
            }

        	return true;

        } catch(\Exception $error) {

        	$this->setLastError($error->getMessage());
            return false;

        }
        
    }

    public function getImageTags($id) {

        if (!$id) { return null; }

        $sql = new \Zend\Db\Sql\Sql($this->dbAdapter);

        $select = $sql->select()
            ->from(['t' => 'tag'])
            ->join(['ti' => 'image_has_tag'],
                'ti.tag_id = t.id',
                [],
                \Zend\Db\Sql\Select::JOIN_LEFT
            )
            ->columns([
                'id' => 't.id'
            ], false)
            ->where(['ti.image_id' => $id]);

        try {
            if ($result = $sql->prepareStatementForSqlObject($select)->execute()) {
                $resultArray = (new \Zend\Db\ResultSet\ResultSet())->initialize($result)->toArray();
                return array_map(function($i) { return $i['id'] * 1; }, $resultArray);
            }
        } catch (\Exception $error) {
            $this->setLastError($error);
        }

        return null;

    }

    public function setLastError($error) {
    	$this->lastError = $error;
    }

    public function getLastError() {
    	return $this->lastError;
    }

    public function setDbAdapter($dbAdapter) {
        $this->dbAdapter = $dbAdapter;
    }

    public function getDbAdapter() {
        return $this->dbAdapter;
    }



    public function getImagesByTags($tags = [], $gallery = null) {

        if (count($tags) == 0) { return []; }

        $select = $sql->select()
            ->from(['i' => 'image'])
            ->join(
                ['it' => 'image_has_tag'],
                'i.id = it.image_id',
                [],
                \Zend\Db\Sql\Select::JOIN_LEFT
                )
            // ->join(
            //     ['t' => 'tag'],
            //     'it.tag_id = t.id',
            //     [],
            //     \Zend\Db\Sql\Select::JOIN_LEFT
            //     )
            ->columns(
                [
                    'id' => 'p.id',
                    'title' => 'p.title',
                    'description' => 'p.description',
                    'category_id' => 'p.category_id',
                    'image_id' => 'p.image_id',
                    'original_src' => 'i.original_src',
                    'preview_src' => 'i.preview_src',
                    'thumbnail_src' => 'i.thumbnail_src'
                ],
                false
            )
            ->where([
                'p.category_id' => $categoryId,
                'p.id' => $randomIds,
                (new \Zend\Db\Sql\Predicate\Predicate())->notEqualTo('p.id', $productId)
            ]);
    }

    public function changeImageOrder($item, $target) {

        $sql = 'select id, gallery_id from image where `order` = ?';
        $dbAdapter = $this->getDbAdapter();

        try {
            $result = $dbAdapter->query($sql, [$item]);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
            return false;
        }

        $dbAdapter->getDriver()->getConnection()->beginTransaction();

        $idArr = $result->toArray();

        if (count($idArr) > 1) {
            $this->setLastError('Two or more images with order ' . $item . ' found!');
            return false;
        }
        if (!isset($idArr[0]['id'])) {
            $this->setLastError('Item with order ' . $item . ' not found!');
            return false;
        }

        $id = $idArr[0]['id'];
        $galleryId = $idArr[0]['gallery_id'];

        try {
            if ($item > $target) {
                $sql = 'update image set `order` = `order` + 1 where `order` >= ? and `order` < ? and id != ? and gallery_id = ?';

                $result = $dbAdapter->query($sql, [$target, $item, $id, $galleryId]);
            } else {
                $sql = 'update image set `order` = `order` - 1 where `order` <= ? and `order` > ? and id != ? and gallery_id = ?';

                $result = $dbAdapter->query($sql, [$target, $item, $id, $galleryId]);
            }

            $sql = 'update image set `order` = ? where id = ?';
            $result = $dbAdapter->query($sql, [$target, $id]);

        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
            $dbAdapter->getDriver()->getConnection()->rollback();
            return false;
        }

        $dbAdapter->getDriver()->getConnection()->commit();

        return true;
    }

    
}