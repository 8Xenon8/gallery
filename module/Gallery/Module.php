<?php
namespace Gallery;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Debug\Debug;
use Zend\Stdlib\Hydrator\ClassMethods;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ControllerProviderInterface, ServiceProviderInterface
{

    public function onBootstrap($e) {

        $serviceManager = $e->getApplication()->getServiceManager();
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $tagMapper = $serviceManager->get('Gallery\Mapper\TagMapper');

        $viewModel->tags = $tagMapper->getEntityList()->toArray();
        $viewModel->loggedIn = ((new \Zend\Session\Container('auth'))->offsetGet('auth') == 'true');
    }

    public function getControllerConfig()
    {
        return [
            'invokables' => array(
                'Gallery\Controller\Index'     => 'Gallery\Controller\Index',
                'Gallery\Controller\Admin'     => 'Gallery\Controller\Admin',
                'Gallery\Controller\Api'       => 'Gallery\Controller\Api', 
            ),
            'factories' => array(
                'Gallery\Controller\IndexController' => function($sl) {

                    $ctr = $sl->getServiceLocator()->get('Gallery\Controller\Index');

                    return $ctr;
                },
                'Gallery\Controller\ApiController' => function($sl) {

                    $ctr = $sl->getServiceLocator()->get('Gallery\Controller\Api');

                    return $ctr;
                },

                'Gallery\Controller\AdminController' => function($sl) {

                    $ctr = $sl->getServiceLocator()->get('Gallery\Controller\Admin');

                    return $ctr;
                },
            ),

        ];
    }

    public function getServiceConfig()
    {
        return [
            'invokables' => [
                'Gallery\Form\Gallery' => 'Gallery\Form\Gallery',
                'Gallery\Form\Upload'  => 'Gallery\Form\Upload',
                
                // //Mapper
                // 'Gallery\Mapper\Day' => 'Gallery\Mapper\Day',
                // 'Gallery\Mapper\Time'               => 'Gallery\Mapper\Time',
                // 'Gallery\Mapper\Entry'                 => 'Gallery\Mapper\Entry',
                // 'Gallery\Mapper\Order'                      => 'Gallery\Mapper\Order',

                'Gallery\Mapper\Gallery'         => 'Gallery\Mapper\Gallery',
                'Gallery\Mapper\Image'           => 'Gallery\Mapper\Image',
                'Gallery\Mapper\Tag'             => 'Gallery\Mapper\Tag',
                'Gallery\Mapper\ImageHasTag'     => 'Gallery\Mapper\ImageHasTag',
                
                // //Entity
                // 'Gallery\Entity\Day'   => 'Gallery\Entity\Day',
                // 'Gallery\Entity\Time'  => 'Gallery\Entity\Time',
                // 'Gallery\Entity\Entry' => 'Gallery\Entity\Entry',

                'Gallery\Entity\Gallery'       => 'Gallery\Entity\Gallery',
                'Gallery\Entity\Image'         => 'Gallery\Entity\Image',
                'Gallery\Entity\Tag'           => 'Gallery\Entity\Tag',
                'Gallery\Entity\ImageHasTag'   => 'Gallery\Entity\ImageHasTag',

                // 'Gallery\Entity\Order'                      => 'Gallery\Entity\Order',
                
                // //Model  
                'Gallery\Model\Image'    => 'Gallery\Model\Image',
                // 'Gallery\Model\AverageItemCost'             => 'Gallery\Model\AverageItemCost',
                // 'Gallery\Model\OrderStory'                  => 'Gallery\Model\OrderStory',
                // 'Gallery\Model\SendNotifications'           => 'Gallery\Model\SendNotifications',
                // 'Gallery\Model\Penalty'                     => 'Gallery\Model\Penalty',
                // 'Gallery\Model\Response'                    => 'Gallery\Model\Response',
                // 'Gallery\Model\Request'                     => 'Gallery\Model\Request',
                // 'Gallery\Model\GalleryRating'                  => 'Gallery\Model\GalleryRating',
                // 'Gallery\Model\Order'                       => 'Gallery\Model\Order',

                // //Navigation
                // 'Gallery\Navigation\Main'         => 'Gallery\Navigation\Main',
            ],
            
            'factories' => [
                'Gallery\Form\GalleryForm' => function(ServiceLocatorInterface $sm) {
                    $form = $sm->get('Gallery\Form\Gallery');

                    // $days = array_map(function($i) {
                    //     return $i['day'];
                    // }, $sm->get('Gallery\Mapper\DayMapper')->getEntityList()->toArray());

                    // $days = array_map(function($i) {
                    //     preg_match('/[1-9]\d*$/', $i, $match);
                    //     return $match[0] . ' ноября';
                    // }, array_combine($days, $days));

                    // $days = array_merge(['' => 'Выберите день'], $days);

                    // $times = array_map(function($i) {
                    //     return $i['time'];
                    // }, $sm->get('Gallery\Mapper\TimeMapper')->getEntityList()->toArray());

                    // $times = array_map(function($i) {
                    //     preg_match('/[1-9]?\d:\d\d/', $i, $match);
                    //     return $match[0];
                    // }, array_combine($times, $times));

                    // $times = array_merge(['' => 'Выберите время'], $times);
                    
                    // $form->get('day')->setValueOptions($days);
                    // $form->get('time')->setValueOptions($times);

                    return $form;
                },

                'Gallery\Form\UploadForm' => function(ServiceLocatorInterface $sm) {
                    $form = $sm->get('Gallery\Form\Upload');
                    return $form;
                },

                // 'Gallery\Mapper\DayMapper' => function (ServiceLocatorInterface $SL) {
                //     $mapper = $SL->get('Gallery\Mapper\Day');
                //     $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                //     $mapper->setEntityPrototype($SL->get('Gallery\Entity\Day'));
                //     $mapper->setHydrator(new ClassMethods());
                //     return $mapper;
                // },

                // 'Gallery\Mapper\TimeMapper' => function (ServiceLocatorInterface $SL) {
                //     $mapper = $SL->get('Gallery\Mapper\Time');
                //     $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                //     $mapper->setEntityPrototype($SL->get('Gallery\Entity\Time'));
                //     $mapper->setHydrator(new ClassMethods());
                //     return $mapper;
                // },

                // 'Gallery\Mapper\EntryMapper' => function (ServiceLocatorInterface $SL) {
                //     $mapper = $SL->get('Gallery\Mapper\Entry');
                //     $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                //     $mapper->setEntityPrototype($SL->get('Gallery\Entity\Entry'));
                //     $mapper->setHydrator(new ClassMethods());
                //     return $mapper;
                // },

                'Gallery\Mapper\GalleryMapper' => function (ServiceLocatorInterface $SL) {
                    $mapper = $SL->get('Gallery\Mapper\Gallery');
                    $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $mapper->setEntityPrototype($SL->get('Gallery\Entity\Gallery'));
                    $mapper->setHydrator(new ClassMethods());
                    return $mapper;
                },

                'Gallery\Mapper\ImageMapper' => function (ServiceLocatorInterface $SL) {
                    $mapper = $SL->get('Gallery\Mapper\Image');
                    $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $mapper->setEntityPrototype($SL->get('Gallery\Entity\Image'));
                    $mapper->setHydrator(new ClassMethods());
                    return $mapper;
                },

                'Gallery\Mapper\TagMapper' => function (ServiceLocatorInterface $SL) {
                    $mapper = $SL->get('Gallery\Mapper\Tag');
                    $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $mapper->setEntityPrototype($SL->get('Gallery\Entity\Tag'));
                    $mapper->setHydrator(new ClassMethods());
                    return $mapper;
                },

                'Gallery\Mapper\ImageHasTagMapper' => function (ServiceLocatorInterface $SL) {
                    $mapper = $SL->get('Gallery\Mapper\ImageHasTag');
                    $mapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $mapper->setEntityPrototype($SL->get('Gallery\Entity\ImageHasTag'));
                    $mapper->setHydrator(new ClassMethods());
                    return $mapper;
                },

                'Gallery\Model\ImageModel' => function(ServiceLocatorInterface $SL) {

                    $model = $SL->get('Gallery\Model\Image');

                    $model->setDbAdapter(
                        $SL->get('Zend\Db\Adapter\Adapter')
                    );

                    return $model;
                },
            ],
        ];
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // Autoload all classes from namespace 'Admin' from '/module/Admin/src/Admin'
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                )
            )
        );
    }
}