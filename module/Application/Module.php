<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class Module
{


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    function onBootstrap($e) {

        $application = $e->getApplication();
        $eventManager = $application->getEventManager();
        // $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this,'onDispatchError'), 100); 

        $eventManager        = $e->getApplication()->getEventManager();
        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);

            // $contollerName = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $contollerName = explode('\\', $controllerClass)[count(explode('\\', $controllerClass)) - 1];

            $config          = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['view_manager']['controller_layouts'][$contollerName])) {
                $controller->layout($config['view_manager']['controller_layouts'][$contollerName]);
            }
        }, 100);
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);


        // $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {

        //         if ($e->getTarget()->)

        //         $e->getApplication()->getServiceManager()->get('ControllerPluginManager')->get('layout')->setTemplate('layout/empty');

        // }, 101);

        $eventManager->attach(MvcEvent::EVENT_RENDER, function($e) {

            // $e->setViewModel(); die;

            $viewModel = $e->getViewModel();

            // var_dump($viewModel->getChildren()[0]->getVariables());

            if ($viewModel instanceof JsonModel) {


                // $routeMatch = $e->getRouteMatch();
                // $controller = $routeMatch->getParam('controller');

                // var_dump($controller); die;

                // die('123');
                $e->getApplication()->getServiceManager()->get('ControllerPluginManager')->get('layout')->disableLayout();

            }

        }, 100); 

    }
     
    function onDispatchError($e) {
        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('error/index');
    }

}
